FROM eclipse-temurin:21.0.5_11-jdk-alpine as builder

COPY target/af-connect-services-backend-*.jar /app/app.jar
WORKDIR /app

# List jar modules
RUN jar xf app.jar &&\
    jdeps \
    --ignore-missing-deps \
    --print-module-deps \
    --multi-release 17 \
    --recursive \
    --class-path 'BOOT-INF/lib/*' \
    app.jar > modules.txt

# Create a custom Java runtime
RUN $JAVA_HOME/bin/jlink \
         --add-modules $(cat modules.txt) \
         --add-modules jdk.crypto.ec \
         --add-modules jdk.crypto.cryptoki \
######### ENABLE DEBUGGING ############
#         --add-modules jdk.jdwp.agent \
#######################################
         --strip-debug \
         --no-man-pages \
         --no-header-files \
         --compress=2 \
         --output /javaruntime

# Extract dependencies from the multi layer built jar file
RUN java -Djarmode=layertools -jar app.jar extract

# Build image
FROM alpine:20240807

COPY --chmod=755 entrypoint.sh /entrypoint.sh

ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"

COPY --from=builder /javaruntime $JAVA_HOME

COPY --from=builder app/dependencies/ app/spring-boot-loader/ app/snapshot-dependencies/ app/application/ ./

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]
CMD ["java", "org.springframework.boot.loader.launch.JarLauncher"]
