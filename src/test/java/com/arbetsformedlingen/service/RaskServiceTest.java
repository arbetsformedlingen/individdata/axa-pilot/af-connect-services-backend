package com.arbetsformedlingen.service;

import com.arbetsformedlingen.dto.RegistrationStatus;
import com.arbetsformedlingen.rask.api.DefaultApi;
import com.arbetsformedlingen.rask.model.ArbetsokandeProcessStatusDTOKomposit;
import com.arbetsformedlingen.rask.model.ArbetssokandeKompaktDTO;
import com.arbetsformedlingen.rask.model.ProcessStatus;
import com.arbetsformedlingen.util.RaskApiFactory;
import com.arbetsformedlingen.util.UuidProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class RaskServiceTest {
    private static Stream<Arguments> sokArbetssokandeRequestGenerator() {
        ArbetssokandeKompaktDTO arbSok1 = mock(ArbetssokandeKompaktDTO.class);
        ArbetssokandeKompaktDTO arbSok2 = mock(ArbetssokandeKompaktDTO.class);
        //@formatter:off
        return Stream.of(
                Arguments.of("zero", List.of(), true),
                Arguments.of("one", List.of(arbSok1), false),
                Arguments.of("two", List.of(arbSok1, arbSok2), false)
        );
        //@formatter:on
    }

    @ParameterizedTest
    @MethodSource("sokArbetssokandeRequestGenerator")
    public void personNotFound(String personalNumber, List<ArbetssokandeKompaktDTO> sokArbetssokandeReturnValue, boolean expectedValue) {
        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(sokArbetssokandeReturnValue);
            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act
            RaskService raskService = new RaskService();
            final var returnedValue = raskService.personNotFound(personalNumber);

            // Assert
            verify(api, times(1)).sokArbetssokande(
                    eq(apiConfig.clientId()),
                    eq(apiConfig.clientSecret()),
                    eq(trackingId),
                    eq(apiConfig.afSystemId()),
                    eq(apiConfig.afEndUserId()),
                    eq(apiConfig.afEnvironment()),
                    eq(personalNumber),
                    eq(null),
                    eq(null),
                    eq(null)
                    );


            assertEquals(expectedValue, returnedValue);
        }
    }

    @Test
    public void getRegistrationStatus_whenSokArbetssokandeReturnsEmptyList_thenReturnRegistrationStatusAsNotRegistered() {
        final var personalNumber = "199010102383";

        final var expectedValue = new RegistrationStatus(personalNumber, false);

        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(List.of());
            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act
            RaskService raskService = new RaskService();
            final var returnedValue = raskService.getRegistrationStatus(personalNumber);

            // Assert
            verify(api, times(1)).sokArbetssokande(
                    eq(apiConfig.clientId()),
                    eq(apiConfig.clientSecret()),
                    eq(trackingId),
                    eq(apiConfig.afSystemId()),
                    eq(apiConfig.afEndUserId()),
                    eq(apiConfig.afEnvironment()),
                    eq(personalNumber),
                    eq(null),
                    eq(null),
                    eq(null)
            );


            assertEquals(expectedValue, returnedValue);
        }
    }

    @Test
    public void getRegistrationStatus_whenSokArbetssokandeReturnsMoreThenOneListItem_thenThrowRuntimeException() {
        final var personalNumber = "199010102383";

        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            ArbetssokandeKompaktDTO arbSokDTO1 = mock(ArbetssokandeKompaktDTO.class);
            ArbetssokandeKompaktDTO arbSokDTO2 = mock(ArbetssokandeKompaktDTO.class);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(List.of(arbSokDTO1, arbSokDTO2));
            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act and Assert
            RaskService raskService = new RaskService();

            final var thrown = assertThrows(RuntimeException.class, () -> raskService.getRegistrationStatus(personalNumber));

            assertEquals("expected 1 result from API", thrown.getMessage());

            verify(api, times(1)).sokArbetssokande(
                    eq(apiConfig.clientId()),
                    eq(apiConfig.clientSecret()),
                    eq(trackingId),
                    eq(apiConfig.afSystemId()),
                    eq(apiConfig.afEndUserId()),
                    eq(apiConfig.afEnvironment()),
                    eq(personalNumber),
                    eq(null),
                    eq(null),
                    eq(null)
            );
        }
    }

    @Test
    public void getRegistrationStatus_whenGivenPersonalNumberDoesNotMatchRetrievedPersonalNumber_thenThrowRuntimeException() {
        final var personalNumber = "199010102383";

        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            ArbetssokandeKompaktDTO arbSokDTO = mock(ArbetssokandeKompaktDTO.class);
            final var sokandeId = mock(Objects.class);
            when(sokandeId.toString()).thenReturn("sokandeId");
            when(arbSokDTO.get("sokandeId")).thenReturn(sokandeId);

            final var personnummer = mock(Objects.class);
            when(personnummer.toString()).thenReturn("fejk_personnummer");
            when(arbSokDTO.get("personnummer")).thenReturn(personnummer);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(List.of(arbSokDTO));
            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act and Assert
            RaskService raskService = new RaskService();

            final var thrown = assertThrows(RuntimeException.class, () -> raskService.getRegistrationStatus(personalNumber));

            assertEquals("Personal number mismatch 1!!", thrown.getMessage());
        }
    }

    @Test
    public void getRegistrationStatus_whenGivenPersonalNumberDoesNotMatchRetrievedProcessStatusPersonalNumber_thenThrowRuntimeException() {
        final var personalNumber = "199010102383";

        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            ArbetssokandeKompaktDTO arbSokDTO = mock(ArbetssokandeKompaktDTO.class);
            final var sokandeId = mock(Objects.class);
            when(sokandeId.toString()).thenReturn("sokandeId");
            when(arbSokDTO.get("sokandeId")).thenReturn(sokandeId);

            final var personnummer = mock(Objects.class);
            when(personnummer.toString()).thenReturn(personalNumber);
            when(arbSokDTO.get("personnummer")).thenReturn(personnummer);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(List.of(arbSokDTO));

            final var arbetssokandeProcessStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
            final var processStatusArbetssokande = mock(HashMap.class);
            when(processStatusArbetssokande.get("personnummer")).thenReturn("fejk_personnummer");
            when(arbetssokandeProcessStatus.get("arbetssokande")).thenReturn(processStatusArbetssokande);

            final var processStatusProcessStatus = mock(HashMap.class);
            when(processStatusProcessStatus.get("sparStatus")).thenReturn("sparStatus");
            when(arbetssokandeProcessStatus.get("processStatus")).thenReturn(processStatusProcessStatus);

            when(api.hamtaArbetssokandeProcessStatus(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "sokandeId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    trackingId,
                    null,
                    null)).thenReturn(arbetssokandeProcessStatus);

            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act and Assert
            RaskService raskService = new RaskService();

            final var thrown = assertThrows(RuntimeException.class, () -> raskService.getRegistrationStatus(personalNumber));

            assertEquals("Personal number mismatch 2!!", thrown.getMessage());
        }
    }

    @Test
    public void getRegistrationStatus_whenGivenPersonExistsButIsNotRegistered_thenReturnRegistrationStatusAsNotRegistered() {
        final var personalNumber = "199010102383";

        final var expectedValue = new RegistrationStatus(personalNumber, false);
//        final var expectedValue = new RegistrationStatus(personalNumber, false).withRegistrationDate(LocalDate.parse("2024-02-14"));

        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            ArbetssokandeKompaktDTO arbSokDTO = mock(ArbetssokandeKompaktDTO.class);
            final var sokandeId = mock(Objects.class);
            when(sokandeId.toString()).thenReturn("sokandeId");
            when(arbSokDTO.get("sokandeId")).thenReturn(sokandeId);

            final var personnummer = mock(Objects.class);
            when(personnummer.toString()).thenReturn(personalNumber);
            when(arbSokDTO.get("personnummer")).thenReturn(personnummer);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(List.of(arbSokDTO));

            final var arbetssokandeProcessStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
            final var processStatusArbetssokande = mock(HashMap.class);
            when(processStatusArbetssokande.get("personnummer")).thenReturn(personalNumber);
            when(arbetssokandeProcessStatus.get("arbetssokande")).thenReturn(processStatusArbetssokande);

            final var processStatusProcessStatus = mock(HashMap.class);
            when(processStatusProcessStatus.get("sparStatus")).thenReturn(ProcessStatus.SparStatusEnum.AVAKTUALISERAD.toString());
            when(processStatusProcessStatus.get("registreringsdatum")).thenReturn("2024-02-14");
            when(arbetssokandeProcessStatus.get("processStatus")).thenReturn(processStatusProcessStatus);

            when(api.hamtaArbetssokandeProcessStatus(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "sokandeId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    trackingId,
                    null,
                    null)).thenReturn(arbetssokandeProcessStatus);

            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act
            RaskService raskService = new RaskService();

            final var returnedValue = raskService.getRegistrationStatus(personalNumber);

            // Assert
            verify(api, times(1)).sokArbetssokande(
                    eq(apiConfig.clientId()),
                    eq(apiConfig.clientSecret()),
                    eq(trackingId),
                    eq(apiConfig.afSystemId()),
                    eq(apiConfig.afEndUserId()),
                    eq(apiConfig.afEnvironment()),
                    eq(personalNumber),
                    eq(null),
                    eq(null),
                    eq(null)
            );

            assertEquals(expectedValue, returnedValue);
        }
    }

    @Test
    public void getRegistrationStatus_whenGivenPersonExistsAndIsRegistered_thenReturnRegistrationStatusAsRegistered() {
        final var personalNumber = "199010102383";

        final var expectedValue = new RegistrationStatus(personalNumber, false).withRegistrationDate(LocalDate.parse("2024-02-14"));

        try (MockedStatic<RaskApiFactory> staticRaskApiFactory = mockStatic(RaskApiFactory.class);
             MockedStatic<UuidProvider> staticUuidProvider = mockStatic(UuidProvider.class)
        ) {
            // Arrange
            final var apiConfig = new RaskApiFactory.ApiConfig(
                    "baseUri",
                    "clientId",
                    "clientSecret",
                    "afSystemId",
                    "afEndUserId",
                    "afEnvironment"
            );
            staticRaskApiFactory.when(RaskApiFactory::getApiConfig).thenReturn(apiConfig);

            final var trackingId = "trackingId";
            staticUuidProvider.when(UuidProvider::randomUUID).thenReturn(trackingId);

            ArbetssokandeKompaktDTO arbSokDTO = mock(ArbetssokandeKompaktDTO.class);
            final var sokandeId = mock(Objects.class);
            when(sokandeId.toString()).thenReturn("sokandeId");
            when(arbSokDTO.get("sokandeId")).thenReturn(sokandeId);

            final var personnummer = mock(Objects.class);
            when(personnummer.toString()).thenReturn(personalNumber);
            when(arbSokDTO.get("personnummer")).thenReturn(personnummer);

            DefaultApi api = mock(DefaultApi.class);
            when(api.sokArbetssokande(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "trackingId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    personalNumber,
                    null,
                    null,
                    null
            )).thenReturn(List.of(arbSokDTO));

            final var arbetssokandeProcessStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
            final var processStatusArbetssokande = mock(HashMap.class);
            when(processStatusArbetssokande.get("personnummer")).thenReturn(personalNumber);
            when(arbetssokandeProcessStatus.get("arbetssokande")).thenReturn(processStatusArbetssokande);

            final var processStatusProcessStatus = mock(HashMap.class);
            when(processStatusProcessStatus.get("sparStatus")).thenReturn(ProcessStatus.SparStatusEnum.INSKRIVEN.toString());
            when(processStatusProcessStatus.get("registreringsdatum")).thenReturn("2024-02-14");
            when(arbetssokandeProcessStatus.get("processStatus")).thenReturn(processStatusProcessStatus);

            when(api.hamtaArbetssokandeProcessStatus(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    "sokandeId",
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    trackingId,
                    null,
                    null)).thenReturn(arbetssokandeProcessStatus);

            staticRaskApiFactory.when(RaskApiFactory::getApi).thenReturn(api);

            // Act
            RaskService raskService = new RaskService();

            final var returnedValue = raskService.getRegistrationStatus(personalNumber);

            // Assert
            verify(api, times(1)).sokArbetssokande(
                    eq(apiConfig.clientId()),
                    eq(apiConfig.clientSecret()),
                    eq(trackingId),
                    eq(apiConfig.afSystemId()),
                    eq(apiConfig.afEndUserId()),
                    eq(apiConfig.afEnvironment()),
                    eq(personalNumber),
                    eq(null),
                    eq(null),
                    eq(null)
            );

            assertEquals(expectedValue, returnedValue);
        }
    }

}
