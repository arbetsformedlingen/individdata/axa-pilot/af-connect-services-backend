package com.arbetsformedlingen.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class RegistrationStatusTest {
    @Test
    public void serializeFalseStatus() throws JsonProcessingException {
        final var registrationStatus = new RegistrationStatus("199010102383", false);
        String json = new ObjectMapper().writeValueAsString(registrationStatus);

        assertEquals(JsonPath.read(json, "$.personalNumber"), "199010102383");
        assertFalse((Boolean) JsonPath.read(json, "$.isRegistered"));
    }

    @Test
    public void serializeTrueStatus() throws JsonProcessingException {
        final var registrationStatus = new RegistrationStatus("199010102383", false);
        final var registrationDate = LocalDate.parse("2022-11-04");
        registrationStatus.withRegistrationDate(registrationDate);

        String json = new ObjectMapper().writeValueAsString(registrationStatus);

        assertEquals(JsonPath.read(json, "$.personalNumber"), "199010102383");
        assertFalse((Boolean) JsonPath.read(json, "$.isRegistered"));

        final var t = JsonPath.read(json, "$.registrationDate");
        assertEquals(LocalDate.parse(JsonPath.read(json, "$.registrationDate")), registrationDate);
    }

}
