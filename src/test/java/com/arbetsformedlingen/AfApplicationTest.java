package com.arbetsformedlingen;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.boot.SpringApplication;

import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;

public class AfApplicationTest {
    @Test
    @Tag("UnitTest")
    public void construct() {
        new AfApplication();
    }

    @Test
    public void main() {
        try (MockedStatic<SpringApplication> springApplicationMockedStatic = mockStatic(SpringApplication.class)) {
            final String[] args = new String[] {};

            AfApplication.main(args);

            springApplicationMockedStatic.verify(
                    () -> SpringApplication.run(AfApplication.class, args),
                    times(1)
            );
        }
    }
}
