package com.arbetsformedlingen.logging;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LogMaskerTest {
    @Test
    @Tag("UnitTest")
    public void construct() {
        try {
            new LogMasker();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    @Tag("UnitTest")
    public void mask() {
        String masked = LogMasker.mask("{\\\"foo\\\":\\\"bar\\\"}");
        assertEquals("{\\\"foo\\\":\\\"bar\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedPersonalIdentityNumberIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"personalIdentityNumber\\\":\\\"199010102383\\\"}");
        assertEquals("{\\\"personalIdentityNumber\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenPersonalIdentityNumberIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"personalIdentityNumber\":\"199010102383\"}");
        assertEquals("{\"personalIdentityNumber\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedClientSecretIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"clientSecret\\\":\\\"abcd1234\\\"}");
        assertEquals("{\\\"clientSecret\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenClientSecretIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"clientSecret\":\"abcd1234\"}");
        assertEquals("{\"clientSecret\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedStatusIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"status\\\":\\\"INSKRIVEN\\\"}");
        assertEquals("{\\\"status\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenStatusIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"status\":\"INSKRIVEN\"}");
        assertEquals("{\"status\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedRegistrationDateIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"registrationDate\\\":\\\"2000-01-01\\\"}");
        assertEquals("{\\\"registrationDate\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenRegistrationDateIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"registrationDate\":\"2000-01-01\"}");
        assertEquals("{\"registrationDate\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedStatusChangedDateIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"statusChangedDate\\\":\\\"2000-01-01\\\"}");
        assertEquals("{\\\"statusChangedDate\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenStatusChangedDateIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"statusChangedDate\":\"2000-01-01\"}");
        assertEquals("{\"statusChangedDate\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedUserIdIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"userId\\\":\\\"123\\\"}");
        assertEquals("{\\\"userId\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenUserIdIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"userId\":\"123\"}");
        assertEquals("{\"userId\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenPisaIdIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("pisa_id: 1234");
        assertEquals("pisa_id: REDACTED", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedRefreshTokenIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"refreshToken\\\":\\\"abc123\\\"}");
        assertEquals("{\\\"refreshToken\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenRefreshTokenIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"refreshToken\":\"abc123\"}");
        assertEquals("{\"refreshToken\":\"REDACTED\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedAccessTokenIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\\\"accessToken\\\":\\\"abc123\\\"}");
        assertEquals("{\\\"accessToken\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenAccessTokenIsPresent_replaceWithRedactedText() {
        String masked = LogMasker.mask("{\"accessToken\":\"abc123\"}");
        assertEquals("{\"accessToken\":\"REDACTED\"}", masked);
    }
}
