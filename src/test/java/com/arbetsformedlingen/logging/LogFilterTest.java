package com.arbetsformedlingen.logging;

import com.arbetsformedlingen.util.LoggerSupplier;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class LogFilterTest {
    @Test
    @Tag("UnitTest")
    public void doLog_infoLog() {
        try (MockedStatic<Slf4jMdcUtil> staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class);
             MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class);
             MockedStatic<ApplicationFieldExtractor> staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class);
        ) {
            // Arrange
            final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
            staticLoggerSupplier.when(() -> LoggerSupplier.forClass(LogFilter.class)).thenReturn(mockLoggerSupplier);
            final Logger mockLogger = mock(Logger.class);
            when(mockLoggerSupplier.get()).thenReturn(mockLogger);

            final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
            final ContentCachingResponseWrapper response = mock(ContentCachingResponseWrapper.class);

            when(response.getStatus()).thenReturn(200);
            staticApplicationFieldExtractor.when(() -> ApplicationFieldExtractor.extractMessage(response)).thenReturn("example-application-message");

            // Act
            final LogFilter logFilter = new LogFilter();
            logFilter.doLog(request, response);

            // Assert
            staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.addUrlFields(request), times(1));
            verify(response, times(1)).getStatus();
            staticApplicationFieldExtractor.verify(() -> ApplicationFieldExtractor.extractMessage(response), times(1));
            verify(mockLogger, times(1)).info("example-application-message");
            verify(mockLogger, times(0)).error(anyString());
            staticSlf4jMdcUtil.verify(Slf4jMdcUtil::clearMdc, times(1));
        }
    }

    @Test
    @Tag("UnitTest")
    public void doLog_errorLog() {
        try (MockedStatic<Slf4jMdcUtil> staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class);
             MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class);
             MockedStatic<ApplicationFieldExtractor> staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class);
        ) {
            // Arrange
            final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
            staticLoggerSupplier.when(() -> LoggerSupplier.forClass(LogFilter.class)).thenReturn(mockLoggerSupplier);
            final Logger mockLogger = mock(Logger.class);
            when(mockLoggerSupplier.get()).thenReturn(mockLogger);

            final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
            final ContentCachingResponseWrapper response = mock(ContentCachingResponseWrapper.class);

            when(response.getStatus()).thenReturn(418);
            staticApplicationFieldExtractor.when(() -> ApplicationFieldExtractor.extractMessage(response)).thenReturn("example-application-message");

            // Act
            final LogFilter logFilter = new LogFilter();
            logFilter.doLog(request, response);

            // Assert
            staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.addUrlFields(request), times(1));
            verify(response, times(1)).getStatus();
            staticApplicationFieldExtractor.verify(() -> ApplicationFieldExtractor.extractMessage(response), times(1));
            verify(mockLogger, times(0)).info(anyString());
            verify(mockLogger, times(1)).error("example-application-message");
            staticSlf4jMdcUtil.verify(Slf4jMdcUtil::clearMdc, times(1));
        }
    }
    @Test
    @Tag("UnitTest")
    public void doLog_whenExceptionIsThrown_simpleErrorLog() {
        try (MockedStatic<Slf4jMdcUtil> staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class);
             MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class);
             MockedStatic<ApplicationFieldExtractor> staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class);
        ) {
            // Arrange
            final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
            staticLoggerSupplier.when(() -> LoggerSupplier.forClass(LogFilter.class)).thenReturn(mockLoggerSupplier);
            final Logger mockLogger = mock(Logger.class);
            when(mockLoggerSupplier.get()).thenReturn(mockLogger);

            final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
            final ContentCachingResponseWrapper response = mock(ContentCachingResponseWrapper.class);

            staticSlf4jMdcUtil.when(() -> Slf4jMdcUtil.addUrlFields(request)).thenThrow(new RuntimeException("error"));

            // Act
            final LogFilter logFilter = new LogFilter();
            logFilter.doLog(request, response);

            // Assert
            staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.addUrlFields(request), times(1));
            verify(response, times(0)).getStatus();
            staticApplicationFieldExtractor.verify(() -> ApplicationFieldExtractor.extractMessage(response), times(0));
            verify(mockLogger, times(0)).info(anyString());
            verify(mockLogger, times(1)).error("Failed to log!");
            staticSlf4jMdcUtil.verify(Slf4jMdcUtil::clearMdc, times(2));
        }
    }

    @Test
    @Tag("UnitTest")
    public void doFilterInternal() throws ServletException, IOException {
        // Arrange
        final List<ContentCachingRequestWrapper> constructionContentCachingRequestWrapperInstances = new ArrayList<>();
        final Map<ContentCachingRequestWrapper, List<?>>constructionContentCachingRequestWrapperArgs = new HashMap<>();
        final List<ContentCachingResponseWrapper> constructionContentCachingResponseWrapperInstances = new ArrayList<>();
        final Map<ContentCachingResponseWrapper, List<?>>constructionContentCachingResponseWrapperArgs = new HashMap<>();
        try (
                MockedConstruction<ContentCachingRequestWrapper> constructionContentCachingRequestWrapper = mockConstruction(ContentCachingRequestWrapper.class, (mock, context) -> {
                    constructionContentCachingRequestWrapperInstances.add(mock);
                    constructionContentCachingRequestWrapperArgs.put(mock, context.arguments());
                });
                MockedConstruction<ContentCachingResponseWrapper> constructionContentCachingResponseWrapper = mockConstruction(ContentCachingResponseWrapper.class, (mock, context) -> {
                    constructionContentCachingResponseWrapperInstances.add(mock);
                    constructionContentCachingResponseWrapperArgs.put(mock, context.arguments());
                })
        ) {
            final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
            final HttpServletResponse mockResponse = mock(HttpServletResponse.class);
            final FilterChain mockFilterChain = mock(FilterChain.class);

            // Act
            final LogFilter logFilter = new LogFilter();
            logFilter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

            // Assert
            assertEquals(1, constructionContentCachingRequestWrapper.constructed().size());
            final ContentCachingRequestWrapper mockContentCachingRequestWrapper = constructionContentCachingRequestWrapper.constructed().get(0);
            assertEquals(1, constructionContentCachingRequestWrapperArgs.get(mockContentCachingRequestWrapper).size());
            assertEquals(mockRequest, constructionContentCachingRequestWrapperArgs.get(mockContentCachingRequestWrapper).get(0));
            assertEquals(1, constructionContentCachingResponseWrapper.constructed().size());
            final ContentCachingResponseWrapper mockContentCachingResponseWrapper = constructionContentCachingResponseWrapper.constructed().get(0);
            assertEquals(1, constructionContentCachingResponseWrapperArgs.get(mockContentCachingResponseWrapper).size());
            assertEquals(mockResponse, constructionContentCachingResponseWrapperArgs.get(mockContentCachingResponseWrapper).get(0));
            assertEquals(1, constructionContentCachingRequestWrapperInstances.size());
            verify(mockFilterChain, times(1)).doFilter(mockContentCachingRequestWrapper, mockContentCachingResponseWrapper);
            verify(mockContentCachingResponseWrapper, times(1)).copyBodyToResponse();
        }
    }
}
