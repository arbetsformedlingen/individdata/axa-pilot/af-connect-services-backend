package com.arbetsformedlingen.logging;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.ContentCachingResponseWrapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ApplicationFieldExtractorTest {
    @Test
    @Tag("UnitTest")
    public void construct() {
        new ApplicationFieldExtractor();
    }

    @Test
    @Tag("UnitTest")
    public void extractMessage() {
        // Arrange
        try (MockedStatic<HttpStatus> staticHttpStatus = mockStatic(HttpStatus.class)) {
            final ContentCachingResponseWrapper mockResponse = mock(ContentCachingResponseWrapper.class);
            when(mockResponse.getStatus()).thenReturn(418);
            final HttpStatus mockHttpStatus = mock(HttpStatus.class);
            when(mockHttpStatus.getReasonPhrase()).thenReturn("I'm a teapot");
            staticHttpStatus.when(() -> HttpStatus.valueOf(418)).thenReturn(mockHttpStatus);

            // Act
            final String message = ApplicationFieldExtractor.extractMessage(mockResponse);

            // Assert
            assertEquals("418 - I'm a teapot", message);
        }
    }
}
