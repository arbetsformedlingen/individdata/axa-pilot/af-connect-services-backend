package com.arbetsformedlingen.logging;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.slf4j.MDC;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class Slf4jMdcUtilTest {
    @Test
    public void construct() {
        new Slf4jMdcUtil();
    }

    @Test
    public void addUrlFields() {
        // Arrange
        try (MockedStatic<UrlFieldExtractor> staticUrlFieldExtractor = mockStatic(UrlFieldExtractor.class);
             MockedStatic<MDC> staticMDC = mockStatic(MDC.class)) {
            final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
            staticUrlFieldExtractor.when(() -> UrlFieldExtractor.extractUrlPath(request)).thenReturn("path");
            staticUrlFieldExtractor.when(() -> UrlFieldExtractor.extractUrlQuery(request)).thenReturn("query");
            staticUrlFieldExtractor.when(() -> UrlFieldExtractor.extractUrlFull(request)).thenReturn("full");
            staticUrlFieldExtractor.when(() -> UrlFieldExtractor.extractUrlPort(request)).thenReturn("port");
            staticUrlFieldExtractor.when(() -> UrlFieldExtractor.extractUrlScheme(request)).thenReturn("scheme");

            // Act
            Slf4jMdcUtil.addUrlFields(request);

            // Assert
            staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(5));
            staticMDC.verify(() -> MDC.put("url.path", "path"));
            staticMDC.verify(() -> MDC.put("url.query", "query"));
            staticMDC.verify(() -> MDC.put("url.full", "full"));
            staticMDC.verify(() -> MDC.put("url.port", "port"));
            staticMDC.verify(() -> MDC.put("url.scheme", "scheme"));
        }
    }

    @Test
    public void clearMdc() {
        // Arrange
        try (MockedStatic<MDC> staticMDC = mockStatic(MDC.class)) {
            // Act
            Slf4jMdcUtil.clearMdc();

            // Assert
            staticMDC.verify(MDC::clear, times(1));
        }
    }
}
