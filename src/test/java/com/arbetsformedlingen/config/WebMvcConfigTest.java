package com.arbetsformedlingen.config;

import org.junit.jupiter.api.Test;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import static org.mockito.Mockito.*;

public class WebMvcConfigTest {
    @Test
    public void addCorsMappings_with_extraAllowedOrigins() {
        // Arrange
        final var mockCorsRegistry = mock(CorsRegistry.class);
        final var mockCorsRegistration = mock(CorsRegistration.class);
        when(mockCorsRegistry.addMapping("/**")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedOrigins("http://example.com")).thenReturn(mockCorsRegistration);

        // Act
        final var corsConfig = new WebMvcConfig(new String[]{"http://example.com"});
        corsConfig.addCorsMappings(mockCorsRegistry);

        // Assert
        verify(mockCorsRegistry, times(1)).addMapping("/**");
        verify(mockCorsRegistration, times(1)).allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS");
        verify(mockCorsRegistration, times(1)).allowedOrigins("http://example.com");
    }
    @Test
    public void addCorsMappings_without_extraAllowedOrigins() {
        // Arrange
        final var mockCorsRegistry = mock(CorsRegistry.class);
        final var mockCorsRegistration = mock(CorsRegistration.class);
        when(mockCorsRegistry.addMapping("/**")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedOrigins("http://example.com")).thenReturn(mockCorsRegistration);

        // Act
        final var corsConfig = new WebMvcConfig(null);
        corsConfig.addCorsMappings(mockCorsRegistry);

        // Assert
        verify(mockCorsRegistry, times(0)).addMapping("/**");
        verify(mockCorsRegistration, times(0)).allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS");
        verify(mockCorsRegistration, times(0)).allowedOrigins("http://example.com");
    }
}
