package com.arbetsformedlingen.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class OpenApiConfigTest {

    @Test
    public void api() {
        try (
                MockedConstruction<OpenAPI> mockOpenAPIConstruction = mockConstruction(OpenAPI.class, (mock, context) -> {
                    when(mock.info(any(Info.class))).thenReturn(mock);});
                MockedConstruction<Info> mockInfoConstruction = mockConstruction(Info.class, (mock, context) -> {
                    when(mock.title("Mitt inskrivningsintyg")).thenReturn(mock);
                    when(mock.description("Backend service for the Mitt inskrivningsintyg frontend application")).thenReturn(mock);
                    when(mock.version("1.0")).thenReturn(mock);})
        ) {
            // Arrange
            OpenApiConfig openApiConfig = new OpenApiConfig();

            // Act
            OpenAPI openAPI = openApiConfig.api();

            // Assert
            assertEquals(1, mockInfoConstruction.constructed().size());
            final Info mockInfo = mockInfoConstruction.constructed().get(0);
            verify(mockInfo, times(1)).title("Mitt inskrivningsintyg");
            verify(mockInfo, times(1)).description("Backend service for the Mitt inskrivningsintyg frontend application");
            verify(mockInfo, times(1)).version("1.0");

            assertEquals(1, mockOpenAPIConstruction.constructed().size());
            final OpenAPI mockOpenApi = mockOpenAPIConstruction.constructed().get(0);
            assertEquals(mockOpenApi, openAPI);
            verify(mockOpenApi, times(1)).info(mockInfo);
        }
    }
}
