package com.arbetsformedlingen.util;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UrlFactoryTest {

    @Test
    @Tag("UnitTest")
    public void construct() {
        new UrlFactory();
    }

    @Test
    @Tag("IntegrationTest")
    public void createURL() throws MalformedURLException {
        URL url = UrlFactory.createURL("http://example.url:8080/foo?bar=baz");
        assertEquals("http", url.getProtocol());
        assertEquals("example.url", url.getHost());
        assertEquals(8080, url.getPort());
        assertEquals("/foo", url.getPath());
        assertEquals("bar=baz", url.getQuery());
    }

    @Test
    @Tag("IntegrationTest")
    public void createURL_whenInvalidUrlIsProvided_thenThrowMalformedURLException() {
        assertThrows(MalformedURLException.class, ()-> UrlFactory.createURL("invalid-url"));
    }
}
