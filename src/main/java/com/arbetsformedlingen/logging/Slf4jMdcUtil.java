package com.arbetsformedlingen.logging;

import org.slf4j.MDC;
import org.springframework.web.util.ContentCachingRequestWrapper;

public class Slf4jMdcUtil {
    public static void addUrlFields(ContentCachingRequestWrapper request) {
        MDC.put("url.path", UrlFieldExtractor.extractUrlPath(request));
        MDC.put("url.query", UrlFieldExtractor.extractUrlQuery(request));
        MDC.put("url.full", UrlFieldExtractor.extractUrlFull(request));
        MDC.put("url.port", UrlFieldExtractor.extractUrlPort(request));
        MDC.put("url.scheme", UrlFieldExtractor.extractUrlScheme(request));
    }

    public static void clearMdc() {
        MDC.clear();
    }
}
