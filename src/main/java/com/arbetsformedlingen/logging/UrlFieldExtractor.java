package com.arbetsformedlingen.logging;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.ContentCachingRequestWrapper;

public class UrlFieldExtractor {
    public static String extractUrlPath(ContentCachingRequestWrapper request) {
        return request.getRequestURI();
    }

    public static String extractUrlQuery(ContentCachingRequestWrapper request) {
        return request.getQueryString();
    }

    public static String extractUrlFull(ContentCachingRequestWrapper request) {
        return ServletUriComponentsBuilder.fromRequest(request).build().toString();
    }

    public static String extractUrlPort(ContentCachingRequestWrapper request) {
        return String.format("%d", ServletUriComponentsBuilder.fromRequest(request).build().getPort());
    }

    public static String extractUrlScheme(ContentCachingRequestWrapper request) {
        return ServletUriComponentsBuilder.fromRequest(request).build().getScheme();
    }
}
