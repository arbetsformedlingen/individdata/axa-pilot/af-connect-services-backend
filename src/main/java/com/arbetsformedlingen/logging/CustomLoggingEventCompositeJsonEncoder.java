package com.arbetsformedlingen.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder;
import java.nio.charset.StandardCharsets;

public class CustomLoggingEventCompositeJsonEncoder extends LoggingEventCompositeJsonEncoder {
    @Override
    public byte[] encode(ILoggingEvent event) {
        byte[] bytes = super.encode(event);
        bytes = hookPreMasking(bytes);
        final String rawMessage = new String(bytes);
        final String possiblyMaskedMessage = rawMessage.contains("\"audit\":") ? rawMessage : LogMasker.mask(rawMessage);
        return possiblyMaskedMessage.getBytes(StandardCharsets.UTF_8);
    }

    protected byte[] hookPreMasking(byte[] bytes) {
        return bytes;
    }
}
