package com.arbetsformedlingen.logging;

import org.springframework.http.HttpStatus;
import org.springframework.web.util.ContentCachingResponseWrapper;

public class ApplicationFieldExtractor {
    public static String extractMessage(ContentCachingResponseWrapper response) {
        return String.format("%d - %s",
                response.getStatus(),
                HttpStatus.valueOf(response.getStatus()).getReasonPhrase()
        );
    }
}
