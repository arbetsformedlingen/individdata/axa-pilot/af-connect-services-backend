package com.arbetsformedlingen.logging;

import com.arbetsformedlingen.util.LoggerSupplier;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import java.io.IOException;

@Component
public class LogFilter extends OncePerRequestFilter {
    private final Logger logger;

    public LogFilter() {
        logger = LoggerSupplier.forClass(LogFilter.class).get();
    }

    public void doLog(ContentCachingRequestWrapper request, ContentCachingResponseWrapper response) {
            try {
                Slf4jMdcUtil.addUrlFields(request);

                if (response.getStatus() == 200) {
                    logger.info(ApplicationFieldExtractor.extractMessage(response));
                } else {
                    logger.error(ApplicationFieldExtractor.extractMessage(response));
                }
            } catch (Exception e) {
                Slf4jMdcUtil.clearMdc();
                logger.error("Failed to log!");
            } finally {
                Slf4jMdcUtil.clearMdc();
            }
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
        final ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

        filterChain.doFilter(requestWrapper, responseWrapper);
        doLog(requestWrapper, responseWrapper);
        responseWrapper.copyBodyToResponse();
    }
}
