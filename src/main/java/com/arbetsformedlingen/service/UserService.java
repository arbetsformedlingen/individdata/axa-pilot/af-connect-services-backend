package com.arbetsformedlingen.service;

import com.arbetsformedlingen.generated.entities.User;
import com.arbetsformedlingen.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.UUID;

@Component
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(String id) {
        final var user = new User(id, ZonedDateTime.now());
        return userRepository.save(user);
    }

    public User getUserById(String id) {
        return userRepository.findById(id).orElse(null);
    }
}
