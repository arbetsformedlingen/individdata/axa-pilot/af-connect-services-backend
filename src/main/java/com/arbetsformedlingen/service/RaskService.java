package com.arbetsformedlingen.service;

import com.arbetsformedlingen.dto.RegistrationStatus;
import com.arbetsformedlingen.util.UuidProvider;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;

import static com.arbetsformedlingen.util.RaskApiFactory.getApi;
import static com.arbetsformedlingen.util.RaskApiFactory.getApiConfig;

@Component
public class RaskService {
    public RaskService() {
    }

    public boolean personNotFound(String personalNumber) {
        final var apiConfig = getApiConfig();
        final var api = getApi();
        final var trackingId = UuidProvider.randomUUID();
        final var arbetssokande = api.sokArbetssokande(
                apiConfig.clientId(),
                apiConfig.clientSecret(),
                trackingId,
                apiConfig.afSystemId(),
                apiConfig.afEndUserId(),
                apiConfig.afEnvironment(),
                personalNumber,
                null,
                null,
                null);
        return arbetssokande.isEmpty();
    }

    public RegistrationStatus getRegistrationStatus(String personalNumber) {
        final var apiConfig = getApiConfig();
        final var api = getApi();
        final var registrationStatus = new RegistrationStatus(personalNumber, false);

        final var trackingId = UuidProvider.randomUUID();
        final var arbetssokande = api.sokArbetssokande(
                apiConfig.clientId(),
                apiConfig.clientSecret(),
                trackingId,
                apiConfig.afSystemId(),
                apiConfig.afEndUserId(),
                apiConfig.afEnvironment(),
                personalNumber,
                null,
                null,
                null);

        if (arbetssokande.isEmpty()) {
            return registrationStatus;
        } else if (arbetssokande.size() > 1) {
            throw new RuntimeException("expected 1 result from API");
        } else {
            final var result = arbetssokande.get(0);
            final var sokandeId = result.get("sokandeId").toString();
            final var personnummer = result.get("personnummer").toString();
            if (!personalNumber.equals(personnummer)) {
                throw new RuntimeException("Personal number mismatch 1!!");
            }

            final var arbetssokandeProcessStatus = api.hamtaArbetssokandeProcessStatus(
                    apiConfig.clientId(),
                    apiConfig.clientSecret(),
                    sokandeId,
                    apiConfig.afSystemId(),
                    apiConfig.afEndUserId(),
                    apiConfig.afEnvironment(),
                    trackingId,
                    null,
                    null);

            final var processStatusArbetssokande = (HashMap<String, String>) arbetssokandeProcessStatus.get("arbetssokande");
            final var processStatusPersonnummer = processStatusArbetssokande.get("personnummer");
            if (!personalNumber.equals(processStatusPersonnummer)) {
                throw new RuntimeException("Personal number mismatch 2!!");
            }

            final var processStatus = (HashMap<String, String>) arbetssokandeProcessStatus.get("processStatus");
            final var sparStatus = processStatus.get("sparStatus");
            final var registreringsDatumString = processStatus.get("registreringsdatum");
            final var registreringsDatum = LocalDate.parse(registreringsDatumString);
            final var isRegistered = sparStatus.equals("INSKRIVEN");
            final var registrationDate = isRegistered ? registreringsDatum : null;

            return registrationStatus.withRegistrationDate(registrationDate);
        }
    }
}
