package com.arbetsformedlingen.service;

import com.arbetsformedlingen.exception.ApiException;
import com.arbetsformedlingen.generated.entities.Client;
import com.arbetsformedlingen.generated.entities.Sharing;
import com.arbetsformedlingen.generated.entities.User;
import com.arbetsformedlingen.repository.SharingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.UUID;

import static com.arbetsformedlingen.util.ErrorFactory.createErrorResponse;

@Component
public class SharingService {

    private final SharingRepository sharingRepository;

    @Autowired
    public SharingService(SharingRepository sharingRepository) {
        this.sharingRepository = sharingRepository;
    }

    public Sharing createSharing(String id, User user, Client client, String kind, ZonedDateTime created) {
        final var sharing = new Sharing(id, user, client, kind, created);
        return sharingRepository.save(sharing);
    }

    public Sharing getSharingById(UUID sharingId) {
        return sharingRepository.findById(sharingId.toString()).orElse(null);
    }

    public Sharing updateSharing(UUID id, ZonedDateTime revoked) {

        final var updated = sharingRepository.setRevoked(id.toString(), revoked);

        if (updated < 1) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not updated"));
        }

        return getSharingById(id);
    }
}
