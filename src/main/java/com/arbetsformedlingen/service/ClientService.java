package com.arbetsformedlingen.service;

import com.arbetsformedlingen.generated.entities.Client;
import com.arbetsformedlingen.generated.entities.Redirect;
import com.arbetsformedlingen.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ClientService {
    private static final Logger logger = LoggerFactory.getLogger(ClientService.class);

    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client getClientById(UUID clientId) {
        return clientRepository.findById(clientId.toString()).orElse(null);
    }

    public Client getClientByKey(String apiKey) {
        final var clients = clientRepository.findByKey(apiKey);

        if (clients.size() > 1) {
            logger.warn("Found too many clients with the same key!");
            return null;
        }

        if (clients.size() == 0) {
            return null;
        }

        return clients.get(0);
    }

    public Client createClient(String clientId, String clientKey, String name, String role, String logo, List<String> redirectUrls, ZonedDateTime created) {
        final var client = new Client(clientId, clientKey, name, logo, created);
        client.setRole(role);
        final var redirects = redirectUrls.stream().map(url->new Redirect(client, url)).collect(Collectors.toSet());
        client.setRedirects(redirects);
        return clientRepository.save(client);
    }

    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }

}
