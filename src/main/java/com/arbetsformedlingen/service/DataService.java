package com.arbetsformedlingen.service;

import com.arbetsformedlingen.generated.entities.Sharing;
import com.arbetsformedlingen.repository.SharingRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class DataService {

    public static class RegistrationStatus {
        String personalNumber;
        Boolean status;
    }


    private final SharingRepository sharingRepository;

    @Autowired
    public DataService(SharingRepository sharingRepository) {
        this.sharingRepository = sharingRepository;
    }

    public Object getDataById(UUID sharingId) {
        final var sharing = sharingRepository.findById(sharingId.toString()).orElseThrow(RuntimeException::new);

        return null;
    }

}
