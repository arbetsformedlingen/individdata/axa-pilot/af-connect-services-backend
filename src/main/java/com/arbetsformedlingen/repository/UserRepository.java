package com.arbetsformedlingen.repository;

import com.arbetsformedlingen.generated.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
}
