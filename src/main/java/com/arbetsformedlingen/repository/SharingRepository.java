package com.arbetsformedlingen.repository;

import com.arbetsformedlingen.generated.entities.Sharing;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;
public interface SharingRepository extends JpaRepository<Sharing, String> {
    List<Sharing> findByUserId(String firstName);

    @Transactional
    @Modifying
    @Query("update Sharing sharing set sharing.revoked = :revoked where sharing.id = :id")
    int setRevoked(@Param("id") String id, @Param("revoked") ZonedDateTime revoked);
}
