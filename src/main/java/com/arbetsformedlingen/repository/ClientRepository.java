package com.arbetsformedlingen.repository;

import com.arbetsformedlingen.generated.entities.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientRepository extends CrudRepository<Client, String> {
    List<Client> findByKey(String key);
}
