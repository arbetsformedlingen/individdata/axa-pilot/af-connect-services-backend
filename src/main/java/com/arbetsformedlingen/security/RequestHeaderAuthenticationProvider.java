package com.arbetsformedlingen.security;

import com.arbetsformedlingen.service.ClientService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestHeaderAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private ClientService clientService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String authKey = String.valueOf(authentication.getPrincipal());

        if(StringUtils.isBlank(authKey)) {
            throw new BadCredentialsException("Bad Request Header Credentials");
        }

        final var client = clientService.getClientByKey(authKey);

        final var role = client.getRole();

        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role);

        return new PreAuthenticatedAuthenticationToken(authentication.getPrincipal(), null, List.of(simpleGrantedAuthority));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(PreAuthenticatedAuthenticationToken.class);
    }
}