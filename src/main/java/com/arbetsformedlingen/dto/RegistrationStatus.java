package com.arbetsformedlingen.dto;

import com.arbetsformedlingen.util.CustomLocalDateSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;
import java.util.Objects;


/**
 * registrationStatus
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "personalNumber",
        "isRegistered",
        "registrationDate"
})
public class RegistrationStatus {

    /**
     * The registered swedish personal number.
     * (Required)
     *
     */
    @JsonProperty("personalNumber")
    @JsonPropertyDescription("The registered swedish personal number.")
    @NotNull
    public String personalNumber;
    /**
     * If the person is currently registered or not.
     * (Required)
     *
     */
    @JsonProperty("isRegistered")
    @JsonPropertyDescription("If the person is currently registered or not.")
    @NotNull
    public boolean isRegistered;
    /**
     * The registration date.
     * (Required IF isRegistered is true)
     *
     */
    @JsonProperty("registrationDate")
    @JsonPropertyDescription("The registration date")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    public LocalDate registrationDate;
    /**
     * No args constructor for use in serialization
     *
     */
    public RegistrationStatus() {
    }

    /**
     *
     * @param personalNumber
     * @param isRegistered
     */
    public RegistrationStatus(String personalNumber, boolean isRegistered) {
        super();
        this.personalNumber = personalNumber;
        this.isRegistered = isRegistered;
    }

    public RegistrationStatus withPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
        return this;
    }

    public RegistrationStatus withIsRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
        return this;
    }

    public RegistrationStatus withRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationStatus that = (RegistrationStatus) o;

        if (isRegistered != that.isRegistered) return false;
        if (!personalNumber.equals(that.personalNumber)) return false;
        return Objects.equals(registrationDate, that.registrationDate);
    }

    @Override
    public int hashCode() {
        int result = personalNumber.hashCode();
        result = 31 * result + (isRegistered ? 1 : 0);
        result = 31 * result + (registrationDate != null ? registrationDate.hashCode() : 0);
        return result;
    }
}