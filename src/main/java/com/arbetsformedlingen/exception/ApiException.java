package com.arbetsformedlingen.exception;

import com.arbetsformedlingen.generated.model.ErrorResponse;
import lombok.Getter;

@Getter
public class ApiException extends RuntimeException {
    private final ErrorResponse errorResponse;

    public ApiException(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }
}
