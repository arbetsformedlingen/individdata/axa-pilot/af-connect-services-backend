package com.arbetsformedlingen.controller;

import com.arbetsformedlingen.exception.ApiException;
import com.arbetsformedlingen.generated.api.ClientsApi;
import com.arbetsformedlingen.generated.entities.Redirect;
import com.arbetsformedlingen.generated.model.*;
import com.arbetsformedlingen.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.arbetsformedlingen.util.ErrorFactory.createErrorResponse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin("*")
@Controller
@RequestMapping("/")
public class ClientsController implements ClientsApi {
    @Autowired
    private ClientService clientService;

    private final NativeWebRequest request;

    @Autowired
    public ClientsController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<PostClientResponse> createClient(PostClientRequest clientRequest) {

        final var redirectUrls = clientRequest.getRedirectUrls().stream().map(URI::toString).toList();

        final var saved = clientService.createClient(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                clientRequest.getHumanFriendlyName(),
                clientRequest.getRole().getValue(),
                clientRequest.getLogotypeUrl().toString(),
                redirectUrls,
                ZonedDateTime.now()
        );

        final var self = linkTo(methodOn(ClientsController.class).createClient(clientRequest)).withSelfRel();
        final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));

        final var redirects = saved.getRedirects().stream().map(r->URI.create(r.getUrl())).toList();

        try {
            final var savedClient = new PostClientResponse(
                    links,
                    new ClientId().uuid(UUID.fromString(saved.getId())),
                    saved.getName(),
                    Role.fromValue(saved.getRole()),
                    new URI(saved.getLogotypeUrl()),
                    redirects,
                    saved.getCreated(),
                    saved.getKey()
            );
            return new ResponseEntity<>(savedClient.redirectUrls(redirects), HttpStatus.CREATED);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new ApiException(createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
        }
    }

    @Override
    public ResponseEntity<GetClientResponse> getClient(UUID clientId) {

        final var client = clientService.getClientById(clientId);

        if (client == null) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        final var self = linkTo(methodOn(ClientsController.class).getClient(clientId)).withSelfRel();
        final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));

        final var redirects = client.getRedirects().stream().map(r->URI.create(r.getUrl())).toList();

        try {
            final var clientModel = new GetClientResponse(
                    links,
                    new ClientId(UUID.fromString(client.getId())),
                    client.getName(),
                    List.of(Role.fromValue(client.getRole())),
                    new URI(client.getLogotypeUrl()),
                    redirects,
                    client.getCreated()
            );
            return new ResponseEntity<>(clientModel.redirectUrls(redirects), HttpStatus.OK);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new ApiException(createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
        }

    }

    @Override
    public ResponseEntity<PatchClientResponse> patchClient (UUID clientId, PatchClientRequest patchClientRequest) {

        final var self = linkTo(methodOn(ClientsController.class).patchClient(clientId, patchClientRequest)).withSelfRel();
        final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));

        final var client = clientService.getClientById(clientId);

        if (client == null) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        if (patchClientRequest.getHumanFriendlyName() != null) {
            client.setName(patchClientRequest.getHumanFriendlyName());
        }

        if (patchClientRequest.getLogotypeUrl() != null) {
            client.setLogotypeUrl(patchClientRequest.getLogotypeUrl().toString());
        }

/*
        if (patchClientRequest.getRevoked() != null) {
            client.setRevoked(patchClientRequest.getRevoked());
        }
*/

        if (patchClientRequest.getRedirectUrls() != null) {
            final var redirectUrls = patchClientRequest.getRedirectUrls().stream().map(u->new Redirect(client, u.toString())).collect(Collectors.toSet());
            client.setRedirects(redirectUrls);
        }

        if (patchClientRequest.getApiKey() != null) {
            client.setKey(patchClientRequest.getApiKey());
        }

        final var savedClient = clientService.saveClient(client);

        URI logotypeUrl;
        try {
            logotypeUrl = new URI(savedClient.getLogotypeUrl());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new ApiException(createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
        }

        final var redirectUris = savedClient.getRedirects().stream().map(r->URI.create(r.getUrl())).toList();

        final var clientResponse = new PatchClientResponse(
                links,
                new ClientId(clientId),
                savedClient.getName(),
                logotypeUrl,
                redirectUris,
                savedClient.getCreated()
        );

        return new ResponseEntity<>(clientResponse, HttpStatus.CREATED);
    }
}
