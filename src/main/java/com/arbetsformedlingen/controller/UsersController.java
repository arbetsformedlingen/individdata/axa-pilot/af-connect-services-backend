package com.arbetsformedlingen.controller;

import com.arbetsformedlingen.exception.ApiException;
import com.arbetsformedlingen.generated.api.UsersApi;
import com.arbetsformedlingen.generated.model.*;
import com.arbetsformedlingen.service.ClientService;
import com.arbetsformedlingen.service.RaskService;
import com.arbetsformedlingen.service.SharingService;
import com.arbetsformedlingen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.net.URI;
import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.arbetsformedlingen.util.ErrorFactory.createErrorResponse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin("*")
@Controller
@RequestMapping("/")
public class UsersController implements UsersApi {
    private final NativeWebRequest request;

    @Autowired
    private SharingService sharingService;

    @Autowired
    private UserService userService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private RaskService raskService;

    @Autowired
    public UsersController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<PostUserSharingResponse> postUserSharing(String userId, PostUserSharingRequest sharingRequest) {

        if (raskService.personNotFound(userId)) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        var user = userService.getUserById(userId);

        if (user == null) {
            user = userService.createUser(userId);
        }

        final var client = clientService.getClientById(sharingRequest.getClientId().getUuid());

        final var sharing = sharingService.createSharing(
                UUID.randomUUID().toString(),
                user,
                client,
                sharingRequest.getKind().toString(),
                ZonedDateTime.now());


        final var self = linkTo(methodOn(UsersController.class).getUserSharing(userId, UUID.fromString(sharing.getId()))).withSelfRel();
        final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));


        final var sharingModel = new PostUserSharingResponse(
                links,
                new SharingId().uuid(UUID.fromString(sharing.getId())),
                URI.create(sharing.getKind()),
                new UserId().personnummer(sharing.getUser().getId()),
                new ClientId().uuid(UUID.fromString(sharing.getClient().getId())),
                sharing.getCreated()
        );

        if (sharing.getRevoked() != null) {
            sharingModel.revoked(sharing.getRevoked());
        }

        return new ResponseEntity<>(sharingModel, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<GetUserSharingResponse> getUserSharing(String userId, UUID sharingId) {

        if (raskService.personNotFound(userId)) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        final var sharing = sharingService.getSharingById(sharingId);

        final var self = linkTo(methodOn(UsersController.class).getUserSharing(userId, UUID.fromString(sharing.getId()))).withSelfRel();
        final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));

        final var sharingModel = new GetUserSharingResponse(
                links,
                new SharingId().uuid(UUID.fromString(sharing.getId())),
                URI.create(sharing.getKind()),
                new UserId().personnummer(sharing.getUser().getId()),
                new ClientId().uuid(UUID.fromString(sharing.getClient().getId())),
                sharing.getCreated()
        );

        if (sharing.getRevoked() != null) {
            sharingModel.revoked(sharing.getRevoked());
        }

        return new ResponseEntity<>(sharingModel, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<GetUserSharingResponse>> getUserSharings(String userId) {

        if (raskService.personNotFound(userId)) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        final var user = userService.getUserById(userId);

        if (user == null) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        final var sharings = user.getSharings();

        final var sharingsModel = sharings.stream()
                .map(sharing -> {
                    final var self = linkTo(methodOn(UsersController.class).getUserSharing(sharing.getUser().getId(), UUID.fromString(sharing.getId()))).withSelfRel();
                    final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));

                    final var sharingModel = new GetUserSharingResponse(
                            links,
                            new SharingId().uuid(UUID.fromString(sharing.getId())),
                            URI.create(sharing.getKind()),
                            new UserId().personnummer(sharing.getUser().getId()),
                            new ClientId().uuid(UUID.fromString(sharing.getClient().getId())),
                            sharing.getCreated()
                    );

                    if (sharing.getRevoked() != null) {
                        sharingModel.revoked(sharing.getRevoked());
                    }

                    return sharingModel;
                }).collect(Collectors.toList());


        return new ResponseEntity<>(sharingsModel, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PatchUserSharingResponse> patchUserSharing(String userId, UUID sharingId, PatchUserSharingRequest sharing) {

        if (raskService.personNotFound(userId)) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        if (sharingService.getSharingById(sharingId) == null) {
            throw new ApiException(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
        }

        final var updatedSharing = sharingService.updateSharing(
                sharingId,
                sharing.getRevoked()
        );

        final var self = linkTo(methodOn(UsersController.class).patchUserSharing(userId, sharingId, sharing)).withSelfRel();
        final var links = Map.of(self.getRel().value(), new Link().href(URI.create(self.getHref())));

        var patchResponse = new PatchUserSharingResponse(
                links,
                new SharingId(sharingId),
                URI.create(updatedSharing.getKind()),
                new UserId(userId),
                new ClientId(UUID.fromString(updatedSharing.getClient().getId())),
                updatedSharing.getCreated()
        );

        return new ResponseEntity<>(patchResponse, HttpStatus.OK);
    }
}
