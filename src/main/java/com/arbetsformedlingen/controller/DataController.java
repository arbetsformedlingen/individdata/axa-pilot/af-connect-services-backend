package com.arbetsformedlingen.controller;

import com.arbetsformedlingen.generated.api.DataApi;
import com.arbetsformedlingen.generated.model.GetDataAccessResponse;
import com.arbetsformedlingen.generated.model.GetDataResponse;
import com.arbetsformedlingen.repository.SharingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin("*")
@Controller
@RequestMapping("/")
public class DataController implements DataApi {
    private final NativeWebRequest request;

    @Autowired
    public DataController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<GetDataResponse> getData(UUID statusId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<GetDataAccessResponse> getDataAccess(UUID dataId, UUID accessId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<List<GetDataAccessResponse>> getDataAccesses(UUID sharingId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
