package com.arbetsformedlingen.controller;

import com.arbetsformedlingen.exception.ApiException;
import com.arbetsformedlingen.generated.model.ErrorResponse;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.arbetsformedlingen.util.ErrorFactory.createErrorResponse;

@RestController
@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler implements ErrorController {
    private static final String ERROR_PATH = "/error";

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<?> exception(HttpServletRequest request, ApiException e) {
        final ErrorResponse errorResponse = e.getErrorResponse();
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(HttpServletRequest request, Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected exception occurred"));
    }

    @Hidden
    @RequestMapping(path = ERROR_PATH)
    public ResponseEntity<?> errorHandler() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(createErrorResponse(HttpStatus.NOT_FOUND, "Not found"));
    }
}
