package com.arbetsformedlingen.util;

import com.arbetsformedlingen.generated.model.ErrorResponse;
import org.springframework.http.HttpStatus;

public class ErrorFactory {
    public static ErrorResponse createErrorResponse(HttpStatus httpStatus) {
        return new ErrorResponse(httpStatus.value(), httpStatus.getReasonPhrase());
    }

    public static ErrorResponse createErrorResponse(HttpStatus httpStatus, String message) {
        return createErrorResponse(httpStatus).message(message);
    }
}
