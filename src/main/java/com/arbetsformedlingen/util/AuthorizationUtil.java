package com.arbetsformedlingen.util;

import com.arbetsformedlingen.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Map;

import static com.arbetsformedlingen.util.ErrorFactory.createErrorResponse;

public class AuthorizationUtil {

    public enum SecurityScheme {
        ADMIN,
        FRONTEND,
        CLIENT
    }

    private static final Map<SecurityScheme, List<String>> keys = Map.of(
            SecurityScheme.ADMIN, List.of("admin"),
            SecurityScheme.FRONTEND, List.of("frontend"),
            SecurityScheme.CLIENT, List.of("client")
    );

    public static void authorize(NativeWebRequest nativeWebRequest, List<SecurityScheme> securitySchemes) {
        final var authKey = nativeWebRequest.getHeader("X-Auth-Key");

        if (authKey == null) {
            throw new ApiException(createErrorResponse(HttpStatus.UNAUTHORIZED));
        }

        final var notAuthorized = securitySchemes.stream()
                .filter(keys::containsKey)
                .filter(scheme->keys.get(scheme).contains(authKey))
                .toList()
                .isEmpty();

        if (notAuthorized) {
            throw new ApiException(createErrorResponse(HttpStatus.FORBIDDEN));
        }
    }

}

