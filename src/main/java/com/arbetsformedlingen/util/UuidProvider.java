package com.arbetsformedlingen.util;

import java.util.UUID;

public class UuidProvider {
    private UuidProvider() {

    }

    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }
}
