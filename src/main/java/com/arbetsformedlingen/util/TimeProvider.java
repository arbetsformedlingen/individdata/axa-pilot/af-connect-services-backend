package com.arbetsformedlingen.util;

public class TimeProvider {
    private TimeProvider() {

    }

    public static Long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
