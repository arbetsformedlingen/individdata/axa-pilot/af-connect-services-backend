package com.arbetsformedlingen.util;

import com.arbetsformedlingen.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

import static com.arbetsformedlingen.util.ErrorFactory.createErrorResponse;

public class ControllerUtil {
    public static NativeWebRequest getNativeWebRequest(Optional<NativeWebRequest> optionalNativeWebRequest) {
        if (optionalNativeWebRequest.isEmpty()) {
            throw new ApiException(createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
        }
        return optionalNativeWebRequest.get();
    }
}
