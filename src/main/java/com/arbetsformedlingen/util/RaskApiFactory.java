package com.arbetsformedlingen.util;

import com.arbetsformedlingen.rask.api.DefaultApi;
import com.arbetsformedlingen.rask.client.ApiClient;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("af.rask")
public class RaskApiFactory {
    private static String baseUri;
    private static String clientId;
    private static String clientSecret;
    private static String afSystemId;
    private static String afEndUserId;
    private static String afEnvironment;

    public record ApiConfig(
            String baseUri,
            String clientId,
            String clientSecret,
            String afSystemId,
            String afEndUserId,
            String afEnvironment
            ) {}

    public static ApiConfig getApiConfig() {
        return new ApiConfig(
                baseUri,
                clientId,
                clientSecret,
                afSystemId,
                afEndUserId,
                afEnvironment
        );
    }

    public static DefaultApi getApi() {
        final var apiClient = new ApiClient();
        apiClient.updateBaseUri(getApiConfig().baseUri());
        return new DefaultApi(apiClient);
    }
}
