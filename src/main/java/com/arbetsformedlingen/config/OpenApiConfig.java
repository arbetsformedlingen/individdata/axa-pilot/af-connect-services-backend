package com.arbetsformedlingen.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI api() {
        return new OpenAPI()
            .info(new Info().title("Mitt inskrivningsintyg")
            .description("Backend service for the Mitt inskrivningsintyg frontend application")
            .version("1.0"));
    }
}
