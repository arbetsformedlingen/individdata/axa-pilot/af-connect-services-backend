# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.3.1](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/compare/v1.3.0...v1.3.1) (2024-01-08)


### Bug Fixes

* use wildcard when copying application jar from artifact into the docker image ([b6e0dc6](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/b6e0dc69e87e799642e393014e1232954b5d2efe))

## [1.3.0](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/compare/v1.2.0...v1.3.0) (2023-12-29)


### Features

* created UrlFactory that wraps URL, since mocking the URL class conflicts with Mockito internals ([335a8aa](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/335a8aa836929aa7eb42cd168e66fbffe4fa4f14))

## [1.2.0](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/compare/v1.1.0...v1.2.0) (2023-12-14)


### Features

* suppress 'INFO' logs of Logback upon app startup ([74aa7de](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/74aa7de89cfb2c1722996ecd0480e1f12676920d))
* suppress spring boot banner from app startup log ([960f0e1](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/960f0e1aa06db69faacd1b526980ecd81fe49bc0))


### Bug Fixes

* exceptions details are now logged under field 'error.message', 'error.stack_trace' and 'error.type' ([f45abcc](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/f45abccd29ac086205c0ef7601580fd5e5466f69))
* the LogFilter now outputs all http requests to the log ([dd78529](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/dd7852999159b6bf9019faee399485d073a65073))

## [1.1.0](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/compare/v1.0.7...v1.1.0) (2023-12-13)


### Features

* add timestamp in serialized ServiceConnection ([74437e9](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/74437e95bef93057a8ad65e594a2c475457af1ad))
* include properties url.scheme and url.port in audit log ([fffd8a1](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/fffd8a19bd44f5999f5f6ba69e4a0bfcac377c37))
* log accordingly to ecs schema ([d73dfc2](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/d73dfc2f0f532ac6e0777474149b19959eabd511))


### Bug Fixes

* correct the various audit field values and enrich the objectinfo field with a timestamp column ([87e3805](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/87e380575922c4c962bc684855301de88e259ff8))
* include missing audit.object_pnr field in audit log pattern ([4ee3f62](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/4ee3f62de4afcf0728412ca45d1bb00c18064038))
* reintroduce masking for logging output, except for audit logs ([558944c](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/558944caee23843c6c5a8d7bec52a8fa0fe4b4b5))
* use LoggingEventCompositeJsonEncoder for ECS 1.6.0-af compliant logging ([29928c3](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/29928c3893c68bcaad842092d21918684f83cdc3))

## [1.0.7](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/compare/v1.0.6...v1.0.7) 


### Features

* warn if the client public key is not found in the client key directory ([7b277f0](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/-/commit/7b277f0caf2c696f4c43d0df3aaf5d06050c85ce))

## [1.0.6](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/compare/v1.0.5...v1.0.6) (2023-11-22)


### Bug Fixes

* changed the swagger springdoc dependency to one that is compatible with spring boot 3.x ([b8c3ab8](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/b8c3ab84057e85649dcaf0740d55bb6cff89d6b3))
* hide exception handler '/error' endpoint from published swagger docs ([dfca2f3](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/dfca2f3d22eb82b65c26a429702307d80a1dab11))

## 1.0.5 (2023-11-20)


### Features

* add maven jacoco code coverage plugin ([1424896](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/14248960c4e1acad4407e145bd716b63b7bfa0c0))
* adding proxy settings to make ais request ([fb6773e](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/fb6773e29168aa3deb18b6a6fcfa93b7d062bcb1))
* implement proxy using apache httpclient ([3d42686](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/3d426865fdc6ad4f6499d8dc3b7e4af8099e80c4))
* load client public keys from configured directory ([ba76a5b](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/ba76a5b76eb53d5eace263ac50a704c5ee11e4b8))


### Bug Fixes

* add an extra property to allow extension of allowed origins ([35f4a37](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/35f4a37b3ace3f9400c91fcf84e3f5f97afcf58c))
* add jwt endpoint back ([61f2a54](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/61f2a54ce35896cc7b3cbcb1f5cbbba326c130bf))
* add version to h2 ([bbc2bae](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/bbc2bae2ab4b1f4dd139f14a8c5826cd8d5e5709))
* added jsr310.JavaTimeModule to mapper ([1acbe45](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/1acbe458300dab73abb55cb871646b4c1da3a82e))
* alert coverage ([79b210b](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/79b210b1f0535038fb6f93cccd58c162ad644844))
* broken tests on windows by using platform-independent line separators in tests ([184b52e](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/184b52eee2df642d583577c876a67808a6abc12c))
* check if content-type value contains application/json ([5be0aac](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/5be0aac3cd22b53e50c0655737914e8f15630f2e))
* collapse liquibase changesets into new initial ([238af05](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/238af05d8f281c58cfe8a643aa6004ca464f420e))
* correctly set the dto values by claims ([b459161](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/b459161de93e22df3a007ed5a35ff369193cb9b7))
* do null check ([8bb3c7f](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/8bb3c7f94d771fa971a4bb5322aedbd8f2e2e9b1))
* dto ais coverage ([7f151e5](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/7f151e5451101ff986ceca113f1ceb6b6b20316f))
* ensure that fresh database schemas is setup correctly by using autoIncrement, and that changelog migration checksum is valid ([3da4428](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/3da4428187f478cc3680e80126228a4ffeb5e85a))
* extraAllowedOrigins to be optional ([39c6339](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/39c6339f5eecfe2ea098014c24977eb1253ec8d2))
* failing tests ([cb61886](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/cb618867534b8a85b4f4a948a1fdab00426611b0))
* getAxaprivatekey method for future testing purpose ([bd85f51](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/bd85f51fd506c5fc9d82747cdac4c18ca04c415b))
* handle situation where an existing database uses 'text' columns prior to migration. Added descriptive comment in the initial changeset ([f768346](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/f7683468e3c1f2625904991c79257680fe81ffae))
* headers in AIS request ([2e9c5f0](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/2e9c5f069a4a2eea3dcf0dfb0d94e8cb68269359))
* hide field registeredAF for all protokols as done previously ([38d47e2](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/38d47e26fd757582e3d933dce63065e544bece3a))
* instantiate the response object mapper ([6a28660](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/6a28660181646a454fbdcd1a494e5ef3066a5f9f))
* java17 ([424a10f](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/424a10f411d85479583f7cdf8abd5252bc8c73b6))
* keep the deprecated endpoints for some time ([e79d795](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/e79d79517552987a8a141cede1d6248ae71968e1))
* keep the deprecated endpoints for some time ([0dd98c3](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/0dd98c3239e5a83d619e95520c26eb700944c376))
* key loading ([01dd3e5](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/01dd3e51a2525593fb906e4d6267b3bc7f178666))
* Method with Boolean return type returns explicit null ([6e2ea38](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/6e2ea38b47a6ca5b75edd2bf541657f4a31feb68))
* no try with resources warning ([5f429cd](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/5f429cd6da378d5e85ab6cbc06f77da4e7452b5d))
* populate 'name' and 'registeredaf' fields ([a45d6f1](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/a45d6f1a69a928e0daee0d36d21670b8813b4f29))
* provide example properties for proxy setting ([2b7fe34](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/2b7fe34b099bda28fd7dde70a017fc78823ee75f))
* reduce return statements ([5c5ca73](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/5c5ca73d27547e6b996c5f7320a006c11965b6ef))
* remove admin API and som alerting code ([95b4812](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/95b48121c79e324062808e129fbdefc9da9fee4a))
* remove ais test mode ([87f890f](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/87f890f59e3141570e4e57b6e147dbecc9a51b95))
* remove DataInit ([3e05208](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/3e0520885467627ae28b02af9dfe2b87a1c89105))
* remove unnecessary failing test ([2663ae2](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/2663ae27f7f005687f134df65eb37ff7dbd6b4d9))
* remove unused endpoints ([7335154](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/7335154caf46471405f3b0e7ff6869303606f1fb))
* Removed -o (offline) flag, doesn't work properly w. Spring Boot. ([fb9fb49](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/fb9fb49f779e6d4b6261371d182848a3c1e1c783))
* removing axa private key ([51f1675](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/51f167563c4a46035959f62a0a74c7cc7be16326))
* rename claim from 'loginId' to 'userLoginId' ([eada3e1](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/eada3e12129d4d8f0d46b4aff8142b3067fcdadd))
* retry logic to log the last attempt ([ffad2a0](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/ffad2a0003a67ce0b2edd91924efbb164d2ba71f))
* run tests upon docker build ([aff829e](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/aff829e6b9cb3ac5da2f5f2a385ee6edd8ac45ef))
* set the correct hiberante version to 5.5.4.Final ([2d4d0e3](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/2d4d0e31785580619dd28491817a9db71a4017ef))
* some spotbug issues ([45f1166](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/45f1166e43ad94686e904996e850a671e26d6a4d))
* speed up build ([17ddad2](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/17ddad24860ab626115169c659048b81f1393caf))
* Updated behaviour for giving consents when previous consent exists. ([3a31ee7](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/3a31ee7a91c171415faf7411c6d19b37b0c83ec0))
* upgrade nimbus dependency ([f5cf3c7](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/f5cf3c7316b74c093142ef22df22d1b150158155))
* use CustomObjectMapper ([42de7f2](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/42de7f24d1854f0e8636ccb430816dceab8fb17b))
* use executeQuery instead of AFRestRequest ([3d98703](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/3d987034267773bcd2847349da537bda88e121d4))
* vulnerability internal representation ([08622c4](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend/commit/08622c4ca221b0c254a7e6beee41aa046d089f00))
